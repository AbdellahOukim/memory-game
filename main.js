const gameContainer = document.querySelector(".game-container");

let imageArray = [];
let score = 0 ;

// Get all images from json file

function getImagesData() {
  let request = new XMLHttpRequest();
  request.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      imageArray = JSON.parse(this.responseText);
      createElements(imageArray);
    }
  };
  request.open("GET", "data.json", true);
  request.send();
}

function createElements(arr) {
  for (let i = 0; i < arr.length; i++) {
    let mainDiv = document.createElement("div");
    mainDiv.className = "box";
    let image = document.createElement("img");
    image.src = arr[i].source;
    image.className = ` image img-${arr[i].index}`;
    image.dataset.target = `img-${arr[i].index}`;
    mainDiv.appendChild(image);
    gameContainer.appendChild(mainDiv);
  }
  checker();
}

function checker() {
  let images = document.querySelectorAll(".image");
  let choosen = [];
  let counter = 0;

  images.forEach((image) => {
    image.addEventListener("click", function () {
      counter++;
      choosen.push(image.dataset.target);
      image.classList.add("show");

      if (counter === 2) {
        setTimeout(function () {
          if (choosen[0] !== choosen[1]) {
            let ele_1 = document.querySelectorAll(`.${choosen[0]}`);
            let ele_2 = document.querySelectorAll(`.${choosen[1]}`);
            ele_1.forEach((ele) => {
              ele.classList.remove("show");
            });
            ele_2.forEach((ele) => {
              ele.classList.remove("show");
            });
          }
          choosen = [];
          counter = 0;
        }, 2000);
      }
      
    });
  });
}

getImagesData();
